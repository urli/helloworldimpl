/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlint.helloworld.service;


import gov.georgia.cje.wsdl.response._1.*;
import gov.georgia.cje.wsdl.response._1_0.HelloWorldAsyncResponsePortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Implementation class for the HelloWorldAsyncResponsePortType interface generated from the WSDL.
 */
public class HelloWorldServiceImpl implements HelloWorldAsyncResponsePortType {

    public static final Logger LOG = LoggerFactory.getLogger(HelloWorldServiceImpl.class.getName());



    public HelloWorldServiceImpl () {

    }

    @Override
    public ResponseBody helloWorldAsync(RequestBody request) {
        LOG.info("Executing operation helloWorldAsync");

        ObjectFactory factory = new ObjectFactory();
        ResponseBody response = factory.createResponseBody();
        String defaultName = "World";
        String name;

        try{
            name = (request.getName().length()>0) ? request.getName() : defaultName;
            response.setDescription("Hello " + name + ". Thank you for your request of type : " + request.getTypeOfRequest());
            response.setCode("OK");

        } catch (Exception e) {

            LOG.error("Unexpected Error", e);
            response.setDescription("Invoking SOAP service operation: helloWorldAsync. "
                    + "  Exception thrown. Exception Message: " + e.getMessage()
                    + "  Exception cause: " + e.getCause().getMessage());

            response.setCode("FAIL");
        }

        return response;
    }
}