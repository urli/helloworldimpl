/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.urlint.exchange.testhead.service;

import gov.georgia.cje.v1_0.wsdl.helloworld.ObjectFactory;
import gov.georgia.cje.v1_0.wsdl.helloworld.RequestBody;
import gov.georgia.cje.v1_0.wsdl.helloworld.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;
import javax.xml.bind.JAXBException;
import javax.xml.ws.soap.SOAPBinding;
import gov.georgia.cje.v1_0.wsdl.helloworld.HelloWorldServicePortType;


/**
 * Implementation of {@link com.urlint.exchange.testhead.service.ExchangeInterfaceServiceIn} to inter-work with the SOAP web service
 */
@Service
@Import(ClientEndpointConfiguration.class)
public class ExchangeInterfaceServiceInImpl implements ExchangeInterfaceServiceIn {

    private static final Logger LOG = LoggerFactory.getLogger(ExchangeInterfaceServiceInImpl.class);

    @Autowired
    private HelloWorldServicePortType helloWorldServiceClient;

    /**
     * Builds the SOAP message and invokes the web service.
     * @param sendString Any String the client chooses to send
     * @return String
     */
    @Override
    public String request(String sendString) {
        LOG.debug("Rest Service Invoked: ExchangeInterfaceServiceInImpl - SendString=[{}]",
                sendString);
        String result;

        try {
            ObjectFactory factory = new ObjectFactory();
            RequestBody requestBody = new RequestBody();
            requestBody.setTypeOfRequest(sendString);

            // Invoke the SOAP service
            ResponseBody response = this.helloWorldServiceClient.testConnection(requestBody);
            result = "Description: " + response.getDescription()  +  "  Code: " + response.getCode();

        } catch (Exception e) {
            LOG.error("Unexpected Error", e);
            result = "Invoking SOAP service. String received: " + sendString
                    + "  Exception thrown. Exception Message: " + e.getMessage()
                    + "  Exception cause: " + e.getCause().getMessage();
        }

        return result ;
    }
}