/*
 * Copyright (c) 2017 by URL Integration
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *     - Prior written consent by URL Integration is required.
 *     - Redistributions of source code must retain the above copyright notice,
 *        this list of conditions and the following disclaimer.
 *     - Redistributions in binary form must reproduce the above copyright notice,
 *        this list of conditions and the following disclaimer in the documentation
 *        and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVEN
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.urlint.exchange.testhead.service;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import gov.georgia.cje.v1_0.wsdl.helloworld.HelloWorldServicePortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.core.io.ResourceLoader;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.security.KeyStore;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;


/**
 * Configuration class to instantiate the SOAP Hello World Web Service Client.
 */
@Configuration
public class ClientEndpointConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(ClientEndpointConfiguration.class);

    // used by both HTTP and HTTPS
    @Value("${client.endpoint.address}")
    private String address;

    // Begin of code used for HTTPS
    @Value("${client.ssl.trust-store}")
    private String trustStoreFileName;

   @Value("${client.ssl.trust-store-password}")
    private String trustStorePassword;

    private ResourceLoader resourceLoader;

    @Autowired
    public ClientEndpointConfiguration(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    private static TrustManager[] getTrustManagers(KeyStore trustStore) throws NoSuchAlgorithmException, KeyStoreException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory fac = TrustManagerFactory.getInstance(alg);
        fac.init(trustStore);
        return fac.getTrustManagers();
    }

    private static KeyManager[] getKeyManagers(KeyStore keyStore, String keyPassword) throws GeneralSecurityException, IOException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        char[] keyPass = keyPassword != null ? keyPassword.toCharArray() : null;
        KeyManagerFactory fac = KeyManagerFactory.getInstance(alg);
        fac.init(keyStore, keyPass);
        return fac.getKeyManagers();
    }

    private void configureSSLOnTheClient(Object c, String keyStorePath, String keyStorePassword) {
        Client client = ClientProxy.getClient(c);
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();

        try {
            /*
            If you would like to load a file from classpath in a Spring Boot JAR, then you have to use
            the resource.getInputStream() method to retrieve it as a InputStream. If you try to use
            resource.getFile() you will receive an error, because Spring tries to access a file
            system path, but it can not access a path in your JAR. Believe this is a "feature" of Tomcat, the embedded server.
             */

            // getClass without getClassLoader always returns null value
            //  InputStream fis = this.getClass().getResourceAsStream("hw-client-truststore.jks");
            InputStream fis = this.getClass().getClassLoader().getResourceAsStream(trustStoreFileName);

            // Can not use File when running from JAR file. Must use Input Stream (see above) when running from JAR.
            // trustStore = new File(this.getClass().getClassLoader().getResource(keyStorePath).getPath());



            TLSClientParameters tlsParams = new TLSClientParameters();
            //  Set to true to disable Common Name check of your certificate. This should always be false in production.
            // tlsParams.setDisableCNCheck(true);
            tlsParams.setDisableCNCheck(false);

            //This line forces the TLS protocol version. It ran before without this line with a TLS1.2 cert.
            // If you input TLSv1.3 it will fail with the 1.2 cert so can be used to force the protocol.
            tlsParams.setSecureSocketProtocol("TLSv1.2");

            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(fis, keyStorePassword.toCharArray());
            TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustFactory.init(keyStore);
            TrustManager[] tm = trustFactory.getTrustManagers();
            tlsParams.setTrustManagers(tm);

            // why was this loading twice? Wont work with input stream get EOFException. Is this extraneous?
            //  keyStore.load(fis2, keyStorePassword.toCharArray());

            KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyFactory.init(keyStore, keyStorePassword.toCharArray());
            KeyManager[] km = keyFactory.getKeyManagers();
            tlsParams.setKeyManagers(km);
            httpConduit.setTlsClientParameters(tlsParams);
        } catch (Exception e) {
            LOG.error("Unexpected Error in configureSSLOnTheClient", e);
            /* TODO: Don't let this client start up if it throws an exception here.
            Need to handle exception gracefully  since client will start but won't work if you don't.
            */

        }
    }

    /**
     * Instantiate the HelloWorld Client using HTTPS.
     * Client uses classes generated in the hello-world-binding>target>generated-sources>wsdl2java
     */
    @Bean (name = "helloWorldServiceClient")
    public HelloWorldServicePortType helloWorldServiceClient(
            @Value("${client.endpoint.address}") String serviceAddress,
            @Value("${client.ssl.trust-store}") String keyStorePath,
            @Value("${client.ssl.trust-store-password}") String keyStorePassword) {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setAddress(serviceAddress);
        jaxWsProxyFactoryBean.setBindingId("http://www.w3.org/2003/05/soap/bindings/HTTP/"); // SOAP 1.2
        HelloWorldServicePortType helloWorldServiceClient = jaxWsProxyFactoryBean.create(HelloWorldServicePortType.class);
        configureSSLOnTheClient(helloWorldServiceClient, keyStorePath, keyStorePassword);
        return helloWorldServiceClient;
    }
    //End of code used for HTTPS

    /**
     * Instantiate the HelloWorld Client using HTTP.
     * Client uses classes generated in the hello-world-binding>target>generated-sources>wsdl2java
     */
    /*
    @Bean(name = "helloWorldServiceClient")
    public HelloWorldServicePortType helloWorldServiceClient() {

        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setAddress(address);
        jaxWsProxyFactoryBean.setBindingId("http://www.w3.org/2003/05/soap/bindings/HTTP/");//soap 1.2
        jaxWsProxyFactoryBean.setServiceClass(HelloWorldServicePortType.class);

        return (HelloWorldServicePortType) jaxWsProxyFactoryBean.create();

    }
    */
}
