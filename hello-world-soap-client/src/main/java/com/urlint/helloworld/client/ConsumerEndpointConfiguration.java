package com.urlint.helloworld.client;

import gov.georgia.cje.v1_0.wsdl.helloworld.HelloWorldServicePortType;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class ConsumerEndpointConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(ConsumerEndpointConfiguration.class);

    // Used by both HTTP and HTTPS
    @Value("${client.endpoint.address}")
    private String address;

    // Beginning of code used for HTTPS
    @Value("${client.ssl.trust-store}")
    private Resource trustStore;

    @Value("${client.ssl.trust-store-password}")
    private String trustStorePassword;

    private static TrustManager[] getTrustManagers(KeyStore trustStore) throws NoSuchAlgorithmException, KeyStoreException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory fac = TrustManagerFactory.getInstance(alg);
        fac.init(trustStore);
        return fac.getTrustManagers();
    }

    private static KeyManager[] getKeyManagers(KeyStore keyStore, String keyPassword) throws GeneralSecurityException, IOException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        char[] keyPass = keyPassword != null ? keyPassword.toCharArray() : null;
        KeyManagerFactory fac = KeyManagerFactory.getInstance(alg);
        fac.init(keyStore, keyPass);
        return fac.getKeyManagers();
    }

    private void configureSSLOnTheClient2(Object c, String keyStorePath, String keyStorePassword) {
        Client client = ClientProxy.getClient(c);
        HTTPConduit httpConduit = (HTTPConduit) client.getConduit();

        // this always returns null: java.net.URL name = getClass().getResource(keyStorePath);
        // this works and substituted it below: java.net.URL name2 = this.getClass().getClassLoader().getResource(keyStorePath);
        File trustStore;
        try {
            trustStore = new File(this.getClass().getClassLoader().getResource(keyStorePath).getPath());
            TLSClientParameters tlsParams = new TLSClientParameters();

            //  Set to true to disable Common Name check of your certificate. This should always be false in production.
            tlsParams.setDisableCNCheck(false);

            //This line forces the TLS protocol version. It ran before without this line with a TLS1.2 cert.
            // If you input TLSv1.3 it will fail with the 1.2 cert so can be used to force the protocol.
            tlsParams.setSecureSocketProtocol("TLSv1.2");
            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(trustStore), keyStorePassword.toCharArray());
            TrustManagerFactory trustFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustFactory.init(keyStore);
            TrustManager[] tm = trustFactory.getTrustManagers();
            tlsParams.setTrustManagers(tm);
            keyStore.load(new FileInputStream(trustStore), keyStorePassword.toCharArray());
            KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyFactory.init(keyStore, keyStorePassword.toCharArray());
            KeyManager[] km = keyFactory.getKeyManagers();
            tlsParams.setKeyManagers(km);
            httpConduit.setTlsClientParameters(tlsParams);
        } catch (Exception e) {
            LOG.error("Unexpected Error in configureSSLOnTheClient", e);
        }
    }

    /**
     * Instantiate the HelloWorld Client using HTTPS.
     * Client uses classes generated in the hello-world-binding>target>generated-sources>wsdl2java
     */
    /* */
    @Bean (name = "helloWorldServiceClient")
    public HelloWorldServicePortType helloWorldServiceClient(
            @Value("${client.endpoint.address}") String serviceAddress,
            @Value("${client.ssl.trust-store}") String keyStorePath,
            @Value("${client.ssl.trust-store-password}") String keyStorePassword) {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setAddress(serviceAddress);
        jaxWsProxyFactoryBean.setBindingId("http://www.w3.org/2003/05/soap/bindings/HTTP/"); // SOAP 1.2
        HelloWorldServicePortType helloWorldServiceClient = jaxWsProxyFactoryBean.create(HelloWorldServicePortType.class);
        configureSSLOnTheClient2(helloWorldServiceClient, keyStorePath, keyStorePassword);
        return helloWorldServiceClient;
    }

    // End of code used for HTTPS

    /**
     * Instantiate the HelloWorld Client using HTTP.
     * Client uses classes generated in the hello-world-binding>target>generated-sources>wsdl2java
     */
    /* Beginning of  code for HTTP */
    /*
    @Bean(name = "helloWorldServiceClient")
    public HelloWorldServicePortType helloWorldServiceClient() {

        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setAddress(address);
        jaxWsProxyFactoryBean.setBindingId("http://www.w3.org/2003/05/soap/bindings/HTTP/");//soap 1.2
        jaxWsProxyFactoryBean.setServiceClass(HelloWorldServicePortType.class);

        return (HelloWorldServicePortType) jaxWsProxyFactoryBean.create();
    }
    */
    // End code for HTTP

}
