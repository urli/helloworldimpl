package com.urlint.helloworld.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.urlint"})
public class HelloWorldSoapClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldSoapClientApplication.class, args);
	}
}
