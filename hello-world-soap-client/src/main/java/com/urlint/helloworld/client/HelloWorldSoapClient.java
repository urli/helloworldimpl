package com.urlint.helloworld.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import gov.georgia.cje.v1_0.wsdl.helloworld.HelloWorldServicePortType;
import gov.georgia.cje.v1_0.wsdl.helloworld.RequestBody;
import gov.georgia.cje.v1_0.wsdl.helloworld.ResponseBody;
import gov.georgia.cje.v1_0.wsdl.helloworld.ObjectFactory;

@Component
public class HelloWorldSoapClient {
    private static final Logger LOG = LoggerFactory.getLogger(HelloWorldSoapClient.class);

    @Autowired
    private HelloWorldServicePortType helloWorldProxy;

    public ResponseBody testConnection(RequestBody request) {
        String requestString = "";
        ObjectFactory factory = new ObjectFactory();
        ResponseBody response = new ResponseBody();

        try {
            requestString = request.getTypeOfRequest();
            response = helloWorldProxy.testConnection(request);


        } catch (Exception e) {
            LOG.error("Unexpected Error", e);
            response.setDescription("Invoking SOAP service. String received: " + requestString
                    + "  Exception thrown. Exception Message: " + e.getMessage()
                    + "  Exception cause: " + e.getCause().getMessage());
            response.setCode("FAIL");
        }

        return response;
    }
}
