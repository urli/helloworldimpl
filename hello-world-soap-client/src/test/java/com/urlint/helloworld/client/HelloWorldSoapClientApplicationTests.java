package com.urlint.helloworld.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import gov.georgia.cje.v1_0.wsdl.helloworld.RequestBody;
import gov.georgia.cje.v1_0.wsdl.helloworld.ResponseBody;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class HelloWorldSoapClientApplicationTests {

	@Autowired
	private HelloWorldSoapClient helloWorldSoapClient;

	@Test
	public void testSayHello() {
		RequestBody request = new RequestBody();
		request.setTypeOfRequest("HEY!");

		ResponseBody response = new ResponseBody();
		response = helloWorldSoapClient.testConnection(request);

		//If endpoint is not running tests will fail and "maven install" will fail. Uncomment when you wish to test.
	//	assertTrue(response.getDescription().equalsIgnoreCase("Type of Request: HEY!"));
	//	assertTrue(response.getCode().equalsIgnoreCase("OK"));
	}

	@Test
	public void contextLoads() {
	}

}
