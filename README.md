## Installing  a Spring Boot java application as a Microsoft Windows Service on a 
## Windows Server Test instance in AWS (Amazon Web Services)

1. Install the required JDK on the server instance. 
2. Update System Environment Variables. Add JAVA_HOME (for example: C:\Program Files\Java\jdk1.8.0_152) 
    to the System variables. Then add %JAVA_HOME%\bin to the System Path variable.
3. Remove any firewalls on you server instance. Search on Firewall and open Windows Firewall
    with Advanced Security. Select link: Windows Firewall Properties. Turn off firewall state
    for all 3 profiles.
3. Create a folder in File Explorer for holding the files needed for creating your Window Service.
4. Information about the application (WINSW) for creating a Windows Service from your JAR file is at: https://github.com/kohsuke/winsw
5. Download the most recent version of WinSW.NET2.exe (for .NET Framework 4) at: https://github.com/kohsuke/winsw/releases/tag/winsw-v2.1.2 
6. Create your winsw.xml configuration file. See: https://github.com/kohsuke/winsw/blob/master/doc/xmlConfigFile.md 
7. Configuration XML files can include environment variable expansions of the form %Name%. 
    Such occurrences, if found, will be automatically replaced by the actual values of the variables. 
    If an undefined environment variable is referenced, no substitution occurs.
8. Rename your downloaded WinSW.NET2.exe to have the same name as your your winsw.xml configuration files.
    For example: hello-world-service-winsw.xml, hello-world-service-winsw.exe
9. Place your application JAR file, application.properties file/s, winws.xml config file, and winws.exe file in folder 
    created in step 3 above. The JAR file contains an embedded Tomcat server that runs the service.
10. Run the command to install the application as a windows service. For example: ./hello-world-service-winsw.exe install
    You can start and stop your application on the command line. For example: ./hello-world-service-winsw.exe start
    Similarly, you may use uninstall, start, stop, etc. YOU MUST RUN INSTALL AS ADMINISTRATOR.
11. You can also start, stop, restart in the Services console as well. 
12. Check for Start errors in the Event Viewer console under Window Logs>Application
13. Other logs will be automatically created in the same folder where you ran the install command.

---

## Example of a winsw.xml type file
## The --httpPort should match the server.port in your Spring application.properties file
## The <name> parameter will display as your service name in the Services Console

<service>
    <id>Hello-World-Service</id>
    <name>Hello-World-Service</name>
    <description>This service runs a Hello World SOAP web service</description>
    <env name="URL_HW_SERVICE_HOME" value="%BASE%"/>
    <executable>java</executable>
    <arguments>-Xrs -Xmx256m -jar "%BASE%\hello-world-service-1.0.0-SNAPSHOT.jar" --httpPort=8089</arguments>
    <logmode>rotate</logmode>
</service>

---

## External configuration and environment profiles in Spring Boot java application.

Your external configuration will not work when starting your application inside the IntelliJ IDE.
You can create a Window Service locally as described above to test external configuration on your local instance.

1. Add a copy of your application.properties file/s to the same location as your JAR file. This will
   override the application.properties file inside your JAR file.
2. Stop your service and edit your properties. Restart your service.
3. For environment profiles create additional property files with naming convention
   application-{profile}.properties. For example: application-test.properties
4. Add these files to your resource directory. If using an external configuration when running a Windows Service
   add the the additional property files to the same location as your external application.properties file.
5. Add the parameter to your application.properties: spring.profiles.active
6. Set the spring.profiles.active parameter value to the profile or profiles you wish to use.
7. Restart your application.
8. More information about Externalized Configuration is at: 
    https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-profile-specific-properties
    
---


## HTTPS - SSL configuration.

1. See the hello-world-testhead-service-tester/src/main/resources/HTTPS Self-Signed Certificates.docx 
   for information on creating self-signed certificates for testing. This document describes using the 
   Cygwin tool which has an openssl version of 1.0.2 which generates TLS1.2 certificates per the client
   specification for TLS1.2. An openssl tool is needed with at least version 1.1.1 to support TLS1.3.
2. The config file referred to in the HTTPS Self-Signed Certificates.docx is located at 
   hello-world-testhead-service-tester/src/main/resources/openssl.cnf
3. SSL configuration is done in the application-test.properties files.
4. Note: There is a quick and dirty way to create self-signed certificates without a database and without x509 commands using just keytool commands. 
   These are not considered true self-signed certificates and are not considered secure. The Cygwin tool at 
   this writing uses openssl library 1.0.2 which is out of date for TLSv1.2. The latest TLSv1.2 
   openssl library is 1.1.0. TLSv1.3 will be supported by openssl 1.1.1 which has not been released.
5. The hello-world-testhead-api/src/main/resources/applications-test.properties explains how to import a public certificate into your 
   JDK keystore for your Soap Client. The information is also in the HTTPS Self-Signed Certificates.docx.
6. The hello-world-testhead-api/src/main/resources/applications-test.properties describes how to create a truststore.jks and password from 
   a certificate file. The properties in this file will need to be updated with the webservice you want 
   to point to. This is also in the HTTPS Self-Signed Certificates.docx.
   
---

